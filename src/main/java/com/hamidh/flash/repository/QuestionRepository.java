package com.hamidh.flash.repository;

import com.hamidh.flash.model.Question;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
@Transactional(readOnly = true)
public class QuestionRepository {

    @PersistenceContext
    private EntityManager entityManager;


    @Transactional
    public Question save(Question entity) {

        entityManager.persist(entity);
        return entity;
    }

    public Question get(long id) {
        return entityManager.find(Question.class, id);
    }


}
