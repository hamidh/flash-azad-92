package com.hamidh.flash.service;

import com.hamidh.flash.account.Account;
import com.hamidh.flash.model.Item;
import com.hamidh.flash.model.Question;
import com.hamidh.flash.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @PostConstruct
    protected void initialize() {

        for (int i = 2; i < 10; i++)
            for (int j = 2; j < 10; j++) {

                Set<String> w = new TreeSet<>();
                while (w.size() < 12) {
                    int wi = 1 + (int) (Math.random() * Math.min(82, Math.min(i, j) * 10));
                    if (wi != i * j)
                        w.add("" + wi);
                }
                String wrongs[] = new String[w.size()];
                questionRepository.save(makeQuestion("&nbsp;&nbsp;&nbsp;&nbsp;" + i + "<BR/>X&nbsp;&nbsp;" + j, w.toArray(wrongs), "" + (i * j)));
            }
    }

    public Question makeQuestion(String title, String[] wrongs, String answer) {
        Arrays.sort(wrongs);
        Question q = new Question();
        q.setDescription(title);
        q.setItems(new ArrayList<Item>());

        for (String w : wrongs) {
            Item item = new Item();
            item.setString(w);
            item.setQuestions(new ArrayList<Question>());
            item.getQuestions().add(q);
            q.getItems().add(item);
        }

        Item item = new Item();
        item.setString(answer);
        q.setCorrectItem(item);
        item.setQuestions1(new ArrayList<Question>());
        item.getQuestions1().add(q);
        System.out.println(q);
        return q;
    }

    public void save(Question entity) {
        questionRepository.save(entity);
    }

    public Question select(Account account) {
//        todo finding rows number
//        TODO id may be not exists in db so loop to load it
        return questionRepository.get((int) (1 + 64 * Math.random()));

    }

    public boolean isCorrect(Long id, int index) {
        Question q = questionRepository.get(id);
        int i = 0;
        for (Item item : q.getItems()) {
            if (item.equals(q.getCorrectItem())) {
                if (i == index)
                    return true;
            }
        }
        return false;

    }

}
