package com.hamidh.flash.rest;

import com.hamidh.flash.model.Item;
import com.hamidh.flash.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Controller
public class QuestionController {
    @Autowired
    QuestionService questionService;
    @Autowired
    QuestionBean questionBean;


    @RequestMapping("question/next/{index}")
    public
    @ResponseBody
    Question greeting(@PathVariable("index") int index) {
        boolean isRight = index == questionBean.getAnswerIndex();
        if (index == -1)
            isRight = true;
        if (isRight) {
            com.hamidh.flash.model.Question questionEntity = questionService.select(null);

            String items[] = new String[questionEntity.getItems().size()];
            Collections.shuffle((List<Item>) questionEntity.getItems());
            int i = 0;
            for (Item item : questionEntity.getItems()) {
                items[i] = item.getString();
                i++;
            }

            items = Arrays.copyOfRange(items, 0, 4);
            questionBean.setAnswerIndex((int) (4 * Math.random()));
            items[questionBean.getAnswerIndex()] = questionEntity.getCorrectItem().getString();
            questionBean.setQuestion(new Question(isRight, questionEntity.getId(), questionEntity.getDescription(), items, questionEntity.isNeedsDelay(), new int[4]));
        } else {
            questionBean.getQuestion().setResult(false);
            questionBean.getQuestion().getHints()[index] = -1;
        }
        return questionBean.getQuestion();
    }

    // Implementing Fisher–Yates shuffle
    static void shuffleArray(Object[] array) {

        int index;
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            if (index != i) {
                Object a = array[index];
                array[index] = array[i];
                array[i] = a;
            }
        }
    }
}