package com.hamidh.flash.rest;

/**
 * Created by hamid on 2/6/14.
 */
public class QuestionBean {
    int answerIndex = 0;
    private Question question;

    public int getAnswerIndex() {
        return answerIndex;
    }

    public void setAnswerIndex(int answerIndex) {
        this.answerIndex = answerIndex;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Question getQuestion() {
        return question;
    }
}
