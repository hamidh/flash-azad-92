package com.hamidh.flash.rest;

import java.util.Arrays;

public class Question {

    private long id;
    private Boolean delayed;
    private Boolean result;
    private String description;
    private String items[];
    private int hints[];

    public Question(Boolean result, long id, String description, String[] items, boolean delayed, int hints[]) {
        this.id = id;
        this.description = description;
        this.items = items;
        this.result = result;
        this.delayed = delayed;
        this.hints = hints;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String[] getItems() {
        return items;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setItems(String[] items) {
        this.items = items;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Boolean getDelayed() {
        return delayed;
    }

    public void setDelayed(Boolean delayed) {
        this.delayed = delayed;
    }

    public int[] getHints() {
        return hints;
    }

    public void setHints(int[] hints) {
        this.hints = hints;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", items=" + Arrays.toString(items) +
                '}';
    }
}
