package com.hamidh.flash.model;

import javax.persistence.*;
import java.util.Collection;

@SuppressWarnings("serial")
@Entity
@Table(name = "item")
public class Item implements java.io.Serializable {

	private Long id;
    private String string;
    private Boolean isImage = false;
    private Collection<Question> questions;
    private Collection<Question> questions1;

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @Column
    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    @Column
    public Boolean getIsImage() {
        return isImage;
    }

    public void setIsImage(Boolean isImage) {
        this.isImage = isImage;
    }
    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "items")
    public Collection<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(Collection<Question> questions) {
        this.questions = questions;
    }

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "correctItem")
    public Collection<Question> getQuestions1() {
        return questions1;
    }

    public void setQuestions1(Collection<Question> questions1) {
        this.questions1 = questions1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;

        Item item = (Item) o;

        if (!isImage.equals(item.isImage)) return false;
        if (!string.equals(item.string)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = string.hashCode();
        result = 31 * result + isImage.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Item{" +
                "string='" + string + '\'' +
                '}';
    }
}
