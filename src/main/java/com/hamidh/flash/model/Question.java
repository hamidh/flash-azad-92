package com.hamidh.flash.model;


import javax.persistence.*;
import java.util.Collection;

@SuppressWarnings("serial")
@Entity
@Table(name = "question")
public class Question implements java.io.Serializable {

	private Long id;
	private String description;
    private Item correctItem;
    private Collection<Item> items;
    boolean needsDelay;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Collection<Item> getItems() {
        return items;
    }

    public void setItems(Collection<Item> items) {
        this.items = items;
    }



    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Item getCorrectItem() {
        return correctItem;
    }

    public void setCorrectItem(Item correctItem) {
        this.correctItem = correctItem;
    }

    @Column()
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column
    public boolean isNeedsDelay() {
        return needsDelay;
    }

    public void setNeedsDelay(boolean needsDelay) {
        this.needsDelay = needsDelay;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", correctItem=" + correctItem +
                ", items=" + items +
                ", needsDelay=" + needsDelay +
                '}';
    }
}
